#Skripta zadatak_1.py ucitava podatkovni skup iz ˇ data_C02_emission.csv.
#Dodajte programski kod u skriptu pomocu kojeg možete odgovoriti na sljede ´ ca pitanja: ´

#a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili ˇ
#duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke veli ˇ cine konvertirajte u tip ˇ
#category.

import pandas as pd
import matplotlib . pyplot as plt
data = pd.read_csv ( 'lv3\data_C02_emission.csv' )

print ( len ( data ))
#print (data.info ())

data.dropna (axis =0)
data.drop_duplicates (inplace=True)
data = data.reset_index ( drop = True )




data['Make']=data['Make'].astype('category')
data['Model']=data['Model'].astype('category')
data['Vehicle Class']=data['Vehicle Class'].astype('category')
data['Transmission']=data['Make']=data['Transmission'].astype('category')
data['Fuel Type']=data['Fuel Type'].astype('category')

#print(data.info())


#b) Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal: ´
#ime proizvoda¯ ca, model vozila i kolika je gradska potrošnja. ˇ

max_potrosnja= data.nlargest(3,'Fuel Consumption City (L/100km)')
print('max:')
print(max_potrosnja[['Make', 'Fuel Consumption City (L/100km)']])


min_potrosnja= data.nsmallest(3,'Fuel Consumption City (L/100km)')
print('min:')
print(min_potrosnja[['Make', 'Fuel Consumption City (L/100km)']])

#c) Koliko vozila ima velicinu motora izme ˇ du 2.5 i 3.5 L? Kolika je prosje ¯ cna C02 emisija ˇ
#plinova za ova vozila?

x= data[( data [ 'Engine Size (L)' ] < 3.5 ) & ( data [ 'Engine Size (L)' ] > 2.4 )]
#x=data[(data['Engine Size (L)'] > 2.4) and (data['Engine Size (L)']<3.5)]
print('x:')
print(x)

print('prosjecna CO2 emisija:   ' ,x['CO2 Emissions (g/km)'].mean())

#d) Koliko mjerenja se odnosi na vozila proizvoda¯ ca Audi? Kolika je prosje ˇ cna emisija C02 ˇ
#plinova automobila proizvoda¯ ca Audi koji imaju 4 cilindara? ˇ

audi=data[data['Make'] == 'Audi']

print('Audi:    ')
print(len(audi))


#e) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na ˇ
#broj cilindara?
cylinders_data = data.groupby('Cylinders')
print("\nNumber of cars depending on number od cylinders: ")
print(cylinders_data['Cylinders'].count())
print("Average CO2 emmision depending on number of cylinders: ")
print(cylinders_data['CO2 Emissions (g/km)'].mean())

#f) Kolika je prosjecna gradska potrošnja u slu ˇ caju vozila koja koriste dizel, a kolika za vozila ˇ
#koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?
disel_cars = data[(data['Fuel Type'] == "D")]
print(f"\nAverage city fuel consumption for disel cars: {disel_cars['Fuel Consumption City (L/100km)'].mean()}")
print(f"Median city fuel consumption for disel cars: {disel_cars['Fuel Consumption City (L/100km)'].median()}")
regular_gasoline_cars = data[(data['Fuel Type'] == "X")]
print(f"Average city fuel consumption for regular gasoline cars: {regular_gasoline_cars['Fuel Consumption City (L/100km)'].mean()}")
print(f"Median city fuel consumption for disel cars: {regular_gasoline_cars['Fuel Consumption City (L/100km)'].median()}")

#g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva? ´
disel_cars = disel_cars[disel_cars['Cylinders'] == 4]
print(f"\nFour(4) cylinder disel car with max city fuel consumption:\n {disel_cars[disel_cars['Fuel Consumption City (L/100km)'] == disel_cars['Fuel Consumption City (L/100km)'].max()]}")

#h) Koliko ima vozila ima rucni tip mjenja ˇ ca (bez obzira na broj brzina)? ˇ

manual_cars = data[data['Transmission'].str.startswith('M')]
print(f"Number of manual cars: {len(manual_cars)}")

#i) Izracunajte korelaciju izme ˇ du numeri ¯ ckih veli ˇ cina. Komentirajte dobiveni rezultat.



print(f"\nCorrelation between numeric data: {data.corr(numeric_only=True)}")


#Pomocu funkcija ´ numpy.array i matplotlib.pyplot pokušajte nacrtati sliku
#2.3 u okviru skripte zadatak_1.py. Igrajte se sa slikom, promijenite boju linija, debljinu linije i
#sl.


import numpy as np
import matplotlib.pyplot as plt


import matplotlib.pyplot as plt
 
# x axis values
x = [1,2,3,3,1]
# corresponding y axis values
y = [1,2,2,1,1]
 
# plotting the points 
plt.plot(x, y, "r", linewidth = 3, marker = "o", markersize = 8)
 
# naming the x axis
plt.xlabel('x')
# naming the y axis
plt.ylabel('y')
 
# giving a title to my graph
plt.title('primjer')

plt.axis([0, 4, 0, 4])
 
# function to show the plot
plt.show()



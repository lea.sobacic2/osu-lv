#Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
#ženama. Skripta zadatak_2.py ucitava dane podatke u obliku numpy polja ˇ data pri cemu je u ˇ
#prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a treci´
#stupac polja je masa u kg.

import numpy as np
import matplotlib.pyplot as plt
import csv
data=np.loadtxt('data.csv',delimiter=',', skiprows=1)

print(data)






#a) Na temelju velicine numpy polja data, na koliko osoba su izvršena mjerenja? ˇ


duljina = len(data[:,0])

print(duljina)

#b) Prikažite odnos visine i mase osobe pomocu naredbe ´ matplotlib.pyplot.scatter.


#plt.scatter(data[1:,1], data[1:,2])
#plt.show()

#c) Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici.

#plt.scatter(data[1::50,1], data[1::50,2])
#plt.show()


#d) Izracunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom ˇ
#podatkovnom skupu.


print('min:',np.min(data[1:,1]))
print('max:', np.max(data[1:,1]))
print('avg:',np.mean(data[1:,1]))

#e) Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili
#muškarce, stvorite polje koje zadrži bool vrijednosti i njega koristite kao indeks retka.
#ind = (data[:,0] == 1)

ind = (data[:,0] == 1)
muskarci = []
zene=[]
print(ind)
heights = data[1:,1]


for i in range(len(heights)):
    if ind[i] == True:
        muskarci.append(heights[i])

    else:
        zene.append(heights[i])


muskarci= np.array(muskarci)
zene= np.array(zene)

print('muskarci:')

print('min:',np.min(muskarci))
print('max:', np.max(muskarci))
print('avg:',np.mean(muskarci))


print('zene:')
print('min:',np.min(zene))
print('max:', np.max(zene))
print('avg:',np.mean(zene))


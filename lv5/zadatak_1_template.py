import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn . metrics import accuracy_score
from sklearn . metrics import confusion_matrix , ConfusionMatrixDisplay
from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import ConfusionMatrixDisplay, classification_report, confusion_matrix
from sklearn.model_selection import train_test_split


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

print(X_train)
print(y_train)
plt.scatter(X_train[:,0],X_train[:,1], c=y_train,s=5,marker='o',cmap="RdBu" )
plt.scatter(X_test[:,0],X_test[:,1], c=y_test,s=10, marker='x',cmap="RdBu")
plt.show()


# inicijalizacija i ucenje modela logisticke regresije
LogRegression_model = LogisticRegression ()
LogRegression_model . fit ( X_train , y_train )
# predikcija na skupu podataka za testiranje
y_test_p = LogRegression_model . predict ( X_test )



t0=LogRegression_model.intercept_
t1=LogRegression_model.coef_[0,0]
t2=LogRegression_model.coef_[0,1]

x=np.linspace(np.min(X_train[:,0]),np.max(X_train[:,0]))
y=-(t2/t1)*x -(t0/t1)
plt.scatter(X_train[:,0],X_train[:,1], c=y_train,s=5,marker='o',cmap="RdBu")
plt.scatter(X_test[:,0],X_test[:,1], c=y_test,s=10, marker='x',cmap="RdBu")
plt.plot(x,y,c="Green")
plt.show()

# tocnost
print (" Tocnost : " , accuracy_score( y_test , y_test_p ) )
# matrica zabune
cm = confusion_matrix ( y_test , y_test_p )
print (" Matrica zabune : " , cm )
disp = ConfusionMatrixDisplay(confusion_matrix ( y_test , y_test_p ) )
disp.plot ()
plt.show ()
# report
print ( classification_report(y_test , y_test_p ) )

tocni=[]
netocni=[]
for i in y_test:
    if y_test[i]==y_test_p[i]:
        tocni.append(y_test_p[i])

    else: netocni.append(y_test_p[i])
    

tocni=np.array(tocni)
netocni=np.array(netocni)


